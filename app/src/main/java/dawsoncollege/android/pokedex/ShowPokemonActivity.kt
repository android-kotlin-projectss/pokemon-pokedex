package dawsoncollege.android.pokedex

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import dawsoncollege.android.pokedex.ShowPokemonActivity.ShowPokemonLoadState.*
import dawsoncollege.android.pokedex.cache.PokemonRoomDatabase
import dawsoncollege.android.pokedex.databinding.ActivityShowPokemonBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShowPokemonActivity : AppCompatActivity() {
    private lateinit var binding: ActivityShowPokemonBinding

    companion object {
        const val POKEDEX_ENTRY_KEY = "pokedex_entry"
    }

    private val GSON: Gson = Gson()

    private lateinit var pokeDexEntry: PokedexEntry

    val database by lazy { PokemonRoomDatabase.getDatabase(this) }


    /**
     * [IN_PROGRESS] The activity is currently loading data from disk or network
     * [COMPLETED] The activity has successfully finished loading the data
     * [FAILED] The activity has finished loading, but the data isn't there
     */
    private enum class ShowPokemonLoadState {
        IN_PROGRESS, COMPLETED, FAILED
    }

    /**
     * Sets the loading state for the activity.
     * This means it shows/hide the appropriate Views, depending on the given current load state.
     */
    private fun setLoadState(state: ShowPokemonLoadState) {
        binding.pokedexNumberTxt.visibility = View.GONE
        binding.dash.visibility = View.GONE
        binding.pokemonNameTxt.visibility = View.GONE
        binding.frontImg.visibility = View.GONE
        binding.backImg.visibility = View.GONE
        binding.pokemonInfoGrid.visibility = View.GONE

        binding.loadingIndicator.visibility = View.GONE

        binding.tryAgainBtn.isEnabled = false
        binding.tryAgainBtn.visibility = View.GONE

        when (state) {
            IN_PROGRESS -> binding.loadingIndicator.visibility = View.VISIBLE
            COMPLETED -> {
                binding.pokedexNumberTxt.visibility = View.VISIBLE
                binding.dash.visibility = View.VISIBLE
                binding.pokemonNameTxt.visibility = View.VISIBLE
                binding.frontImg.visibility = View.VISIBLE
                binding.backImg.visibility = View.VISIBLE
                binding.pokemonInfoGrid.visibility = View.VISIBLE
            }
            FAILED -> {
                binding.tryAgainBtn.isEnabled = true
                binding.tryAgainBtn.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShowPokemonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setLoadState(IN_PROGRESS)

        intent.extras?.let {
            // TODO : get the result from the intent, into a variable
            this.pokeDexEntry =
                GSON.fromJson(it.getString(POKEDEX_ENTRY_KEY), PokedexEntry::class.java)
        }

        loadPokemon()
        binding.tryAgainBtn.setOnClickListener { loadPokemon() }
    }

    // TODO : call this when data was loaded from the local cache
    private fun showLoadFromDbToast() =
        Toast.makeText(
            applicationContext,
            getString(R.string.load_from_db),
            Toast.LENGTH_LONG
        ).show()

    // TODO : call this when data had to be loaded from the network API
    private fun showLoadFromAPIToast() =
        Toast.makeText(
            applicationContext,
            getString(R.string.load_from_api),
            Toast.LENGTH_LONG
        ).show()

    // TODO : call this when data loading from cache and network failed
    private fun showErrorLoadToast() =
        Toast.makeText(
            applicationContext,
            getString(R.string.error_fetch_pokemon),
            Toast.LENGTH_LONG
        ).show()

    private fun loadPokemon() {
        setLoadState(IN_PROGRESS)
        // TODO : try to get the pokemon data (for the pokedex entry received in the intent) from the local database
        var pokemon: Pokemon? = null
        lifecycleScope.launch(Dispatchers.Main) {
            launch(Dispatchers.IO) {
                pokemon = database.pokemonDao().getPokemon(pokeDexEntry.name)
            }
        }.invokeOnCompletion {
            if (pokemon != null) {
                displayPokemon(pokemon!!)
                runOnUiThread { showLoadFromDbToast();setLoadState(COMPLETED)}
            } else {
                // TODO : if necessary get the pokemon data from the web (and cache it in the local database)
                lifecycleScope.launchWhenCreated {
                    val jobPokdexEntries = launch {
                        pokemon = getPokemonAPI(pokeDexEntry.number, pokeDexEntry.name)
                    }
                    jobPokdexEntries.join()
                }.invokeOnCompletion {
                    if (pokemon == null) { runOnUiThread {setLoadState(FAILED) ;showErrorLoadToast() } }
                    else {
                        // Save Pokemon To DB
                        insertPokemonToDatabase(pokemon!!)
                        // TODO: display the pokemon data
                        displayPokemon(pokemon!!)
                        runOnUiThread { setLoadState(COMPLETED);showLoadFromAPIToast()}
                    }
                }
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun displayPokemon(pokemon: Pokemon) {
        // TODO : from the pokemon data that was loaded by [loadPokemon], display it
        binding.pokedexNumberTxt.text = "#${pokemon.number}"/* pokedex entry number e.g "#023" */
        binding.pokemonNameTxt.text = pokemon.name /* pokedex entry name e.g. "pikachu" */

        // Get Images
        // TODO : if the pokemon is loaded, get the sprites from the web
        lifecycleScope.launchWhenCreated {
            launch {
                /* pokemon's front sprite */
                binding.frontImg.setImageDrawable(getPokemonImage(pokemon.frontImgURL))
            }
            launch {
                /* pokemon's back sprite */
                binding.backImg.setImageDrawable(getPokemonImage(pokemon.backImgURL))
            }
        }

        binding.pokemonTypesTxt.text = pokemon.types
        binding.pokemonExpTxt.text = pokemon.exp
        binding.pokemonMaxhpTxt.text = pokemon.maxHp
        binding.pokemonAttTxt.text = pokemon.attStat
        binding.pokemonDefTxt.text = pokemon.defStat
        binding.pokemonSpAttTxt.text = pokemon.spAttStat
        binding.pokemonSpDefTxt.text = pokemon.spDefStat
        binding.pokemonSpeedTxt.text = pokemon.speedStat
    }

    /**
     * This method save pokedex entries to database
     */
    private fun insertPokemonToDatabase(pokemon: Pokemon) {
        lifecycleScope.launchWhenCreated {
            launch(Dispatchers.IO) {
                database.pokemonDao().insertPokemon(pokemon)
            }
        }
    }
}