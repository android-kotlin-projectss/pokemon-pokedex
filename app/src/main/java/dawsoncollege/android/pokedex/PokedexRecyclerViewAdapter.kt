package dawsoncollege.android.pokedex

import android.content.ActivityNotFoundException
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.internal.LinkedTreeMap
import dawsoncollege.android.pokedex.databinding.PokedexItemBinding

// TODO : take a list of pokedex entries as parameter to [PokedexRecyclerViewAdapter]'s constructor
class PokedexRecyclerViewAdapter(
    private val _allPokedexEntries: Array<PokedexEntry>
) :
    RecyclerView.Adapter<PokedexRecyclerViewAdapter.PokedexViewHolder>() {
    private val LOG_TAG = "POKEDEX_ADAPTER"
    private val GSON: Gson = GsonBuilder().setPrettyPrinting().create()

    class PokedexViewHolder(val binding: PokedexItemBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokedexViewHolder =
        PokedexViewHolder(
            PokedexItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: PokedexViewHolder, position: Int) {
        val binding = holder.binding
        val context = binding.root.context
        // TODO : get the pokedex entry from the list that this adapter should have
        val pokedexEntry: Map<String,String> = mapOf(
            Pair("number",_allPokedexEntries[position].number),// _allPokedexEntries[position]["number"].toString().toDouble().toInt().toString()),
            Pair("name", _allPokedexEntries[position].name)//_allPokedexEntries[position]["name"]!!),
            )

        // TODO : change to show the information of the pokedex entry
        binding.pokedexNumberTxt.text = pokedexEntry["number"]
        binding.pokemonNameTxt.text = pokedexEntry["name"]

        binding.infoBtn.setOnClickListener {
            val intent = Intent(context, ShowPokemonActivity::class.java).also {
                it.putExtra(ShowPokemonActivity.POKEDEX_ENTRY_KEY, GSON.toJson(pokedexEntry))
            }

            try {
                context.startActivity(intent)
            } catch (exc: ActivityNotFoundException) {
                Log.w(LOG_TAG, "Could not launch intent ShowPokemon", exc)
                Toast.makeText(
                    context,
                    // TODO : change "pokemon name" to the name of the pokemon
                    context.getString(R.string.error_intent_show_pokemon, "${pokedexEntry["name"]}"),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun getItemCount(): Int = _allPokedexEntries.size // TODO : change that to get the real item count
}