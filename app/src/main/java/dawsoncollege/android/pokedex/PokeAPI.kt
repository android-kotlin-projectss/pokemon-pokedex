package dawsoncollege.android.pokedex

import android.graphics.drawable.Drawable
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

private const val LOG_TAG = "POKEAPI"

const val POKE_API_BASE_URL = "https://pokeapi.co/api/v2"
const val POKEDEX_BASE_URL = "$POKE_API_BASE_URL/pokedex"
const val POKEMON_BASE_URL = "$POKE_API_BASE_URL/pokemon"

private val GSON: Gson = GsonBuilder().setPrettyPrinting().create()


/**
 * This method creates a List<LinkedTreeMap<String,String>>? with the data of a pokemon fetched from API.
 * It contains Pokemon Number and Name.
 * If null -> it means fetch failed
 *
 * @return List<LinkedTreeMap<String,String>>? : List of pokemons names and numbers
 */
suspend fun getAllPokedexEntries(): Array<PokedexEntry>? {
    try {
        var pokedexString: String?
        withContext(Dispatchers.IO) {
            val url = URL("${POKEDEX_BASE_URL}/2")
            pokedexString = fetchDataFromAPI(url)
        }
        if(pokedexString == null){
            throw Exception("Pokedex entries were null in PokeAPI-getAllPokedexEntries")
        }
        return GSON.fromJson(simplifyPokedexEntries(pokedexString!!),
            Array<PokedexEntry>::class.java)
    } catch (e: Exception) {
        Log.e("Exception", e.stackTraceToString())
        return null
    }
}

/**
 * This method creates a JSONObject with the data of a pokemon fetched from API.
 * If null -> it means getting data from API failed
 *
 * @return List<LinkedTreeMap<String,String>>? : List of pokemons names and numbers
 */
suspend fun getPokemonAPI(pokeNumber: String, pokeName: String): Pokemon? {
    var pokemonString: String?
    // Check if it is a number at runtime
    if (pokeNumber.toIntOrNull() == null) {
        throw RuntimeException("Pokemon Number passed to getPokemonApi is null.")
    }
    try {
        withContext(Dispatchers.IO) {
            val url = URL("${POKEMON_BASE_URL}/${pokeNumber}")
            pokemonString = fetchDataFromAPI(url)
        }
        return createPokemonFromJson(
            JSONObject(simplifyPokemon(pokemonString!!)),
            pokeNumber,
            pokeName
        )
    } catch (e: Exception) {
        Log.e("Exception", e.stackTraceToString())
        return null
    }
}

/**
 * This method creates a Data Class Pokemon to holds a pokemon values.
 *
 * @return Pokemon
 */
private fun createPokemonFromJson(pokemonJson: JSONObject, number: String, name: String): Pokemon {
    // Get Types
    var typeStrings: String = ""
    (0 until pokemonJson.getJSONArray("types").length()).forEach {
        typeStrings += "${pokemonJson.getJSONArray("types").get(it)} "
    }

    return Pokemon(
        number,
        name,
        pokemonJson.getString("front_sprite"),
        pokemonJson.getString("back_sprite"),
        typeStrings,
        pokemonJson.getString("base_exp_reward"),
        pokemonJson.getString("base_maxHp"),
        pokemonJson.getString("base_attack"),
        pokemonJson.getString("base_defense"),
        pokemonJson.getString("base_special-attack"),
        pokemonJson.getString("base_special-defense"),
        pokemonJson.getString("base_speed"),
    )
}

/**
 * This method returns an Drawable of an image.

 * @param url URL of the image file
 * @return Drawable? : Drawable if it exists
 */
suspend fun getPokemonImage(url: String): Drawable? {
    var image: Drawable?
    withContext(Dispatchers.IO) {
        image = Drawable.createFromStream(URL(url).content as InputStream, "name")!!
    }
    return image
}


/**
 * This method Abstracts the creation of a JSONObject.
 * Accepts a URL and opens a connection to fetch the data from the api with a GET.
 * Will send null if it is not HTTP_OK
 *
 * @param url : URL of the data to GET
 * @return JSONOBject> : JSONObject with the data or null if Connection failed
 */
private fun fetchDataFromAPI(url: URL): String? {
    val connection = url.openConnection() as HttpURLConnection
    connection.requestMethod = "GET"
    connection.connect()
    var content = ""
    if (connection.responseCode == HttpURLConnection.HTTP_OK) {
        val inStream = connection.inputStream
        val buffer = BufferedReader(inStream.reader())
        // Get content
        buffer.use { buf -> content = buf.readText() }
        // Close stream
        inStream.close()
    }
    // Finish with network
    connection.disconnect()
    // If response is not OK return null
    if (connection.responseCode != HttpURLConnection.HTTP_OK) {
        return null
    }
    return content
}

/**
 * Simplifies the API response from the pokedex endpoint.
 * The simplified JSON has the following format :
 * ```
 * [
 *   {
 *     "number" : 1,
 *     "name" : "bulbasaur"
 *   },
 *   {
 *     "number" : 2,
 *     "name" : "ivysaur"
 *   },
 *   ...
 *   {
 *     "number" : 151,
 *     "name" : "mew"
 *   }
 * ]
 * ```
 *
 * @param apiResponse the API response as received from PokeAPI's pokedex endpoint
 *                    (https://pokeapi.co/api/v2/pokedex)
 *
 * @return the simplified JSON as a String
 */
private fun simplifyPokedexEntries(apiResponse: String): String {
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)

    val simplified = json["pokemon_entries"].asJsonArray.map {
        JsonObject().apply {
            addProperty(
                "number",
                it.asJsonObject["entry_number"].asInt
            )
            addProperty(
                "name",
                it.asJsonObject["pokemon_species"].asJsonObject["name"].asString
            )
        }
    }

    return GSON.toJson(simplified)
}


/**
 * Simplifies the API response from the pokemon endpoint.
 * The simplified JSON has the following format :
 * ```
 * {
 *   "name" : "bulbasaur",
 *   "base_exp_reward" : 64,
 *   "types" : [
 *     "grass", "poison"
 *   ],
 *   "base_maxHp" : 45,
 *   "base_attack" : 49,
 *   "base_defense" : 49,
 *   "base_special-attack" : 65,
 *   "base_special-defense" : 65,
 *   "base_speed" : 45,
 *   "back_sprite" : "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-i/red-blue/transparent/back/1.png"
 *   "front_sprite" : "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-i/red-blue/transparent/1.png"
 * }
 * ```
 *
 * @param apiResponse the API response as received from PokeAPI's pokemon endpoint
 *                    (https://pokeapi.co/api/v2/pokemon)
 *
 * @return the simplified JSON as a String
 */
private fun simplifyPokemon(apiResponse: String): String {
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)

    val simplified = JsonObject().apply {
        addProperty(
            "name",
            json["name"].asString
        )
        addProperty(
            "base_exp_reward",
            json["base_experience"].asInt
        )
        add(
            "types",
            JsonArray().apply {
                json["types"].asJsonArray.map {
                    it.asJsonObject["type"].asJsonObject["name"].asString
                }.forEach { this.add(it) }
            }
        )
        json["stats"].asJsonArray.associate {
            it.asJsonObject.run {
                (this["stat"].asJsonObject["name"].asString) to (this["base_stat"].asInt)
            }
        }.forEach {
            val statName = if (it.key == "hp") "maxHp" else it.key
            this.addProperty("base_$statName", it.value)
        }
        addProperty(
            "back_sprite",
            json["sprites"].asJsonObject["versions"].asJsonObject["generation-i"]
                .asJsonObject["red-blue"].asJsonObject["back_transparent"].asString
        )
        addProperty(
            "front_sprite",
            json["sprites"].asJsonObject["versions"].asJsonObject["generation-i"]
                .asJsonObject["red-blue"].asJsonObject["front_transparent"].asString
        )
    }

    return GSON.toJson(simplified)
}