package dawsoncollege.android.pokedex

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.internal.LinkedTreeMap
import dawsoncollege.android.pokedex.MainActivity.MainActivityLoadState.*
import dawsoncollege.android.pokedex.cache.PokemonRoomDatabase
import dawsoncollege.android.pokedex.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter: PokedexRecyclerViewAdapter

    private var _allPokedexEntries: Array<PokedexEntry>? = null

    val database by lazy { PokemonRoomDatabase.getDatabase(this)}

    private val NUMBER_OF_POKEMONS = 151

    /**
     * [IN_PROGRESS] The activity is currently loading data from disk or network
     * [COMPLETED] The activity has successfully finished loading the data
     * [FAILED] The activity has finished loading, but the data isn't there
     */
    private enum class MainActivityLoadState {
        IN_PROGRESS, COMPLETED, FAILED
    }

    /**
     * Sets the loading state for the activity.
     * This means it shows/hide the appropriate Views, depending on the given current load state.
     */
    private fun setLoadState(state: MainActivityLoadState) {
        binding.loadingIndicator.visibility = View.GONE
        binding.pokedexRecyclerView.visibility = View.GONE
        binding.tryAgainBtn.isEnabled = false
        binding.tryAgainBtn.visibility = View.GONE

        when (state) {
            IN_PROGRESS -> binding.loadingIndicator.visibility = View.VISIBLE
            COMPLETED -> binding.pokedexRecyclerView.visibility = View.VISIBLE
            FAILED -> {
                binding.tryAgainBtn.isEnabled = true
                binding.tryAgainBtn.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadPokedexEntries()

        binding.tryAgainBtn.setOnClickListener { loadPokedexEntries() }
    }

    // TODO : call this when data was loaded from the local cache
    private fun showLoadFromDbToast() =
        Toast.makeText(
            applicationContext,
            getString(R.string.load_from_db),
            Toast.LENGTH_LONG
        ).show()

    // TODO : call this when data had to be loaded from the network API
    private fun showLoadFromAPIToast() =
        Toast.makeText(
            applicationContext,
            getString(R.string.load_from_api),
            Toast.LENGTH_LONG
        ).show()

    // TODO : call this when data loading from cache and network failed
    private fun showErrorLoadToast() =
        Toast.makeText(
            applicationContext,
            getString(R.string.error_fetch_entries),
            Toast.LENGTH_LONG
        ).show()

    private fun loadPokedexEntries() {
        setLoadState(IN_PROGRESS)

        // TODO : try to get the list of pokedex entries from the local database
        lifecycleScope.launchWhenCreated {
            val job = launch(Dispatchers.IO){
                _allPokedexEntries = database.pokedexEntryDao().getAllEntries()
            }
            job.join()
        }.invokeOnCompletion {
            // In case the application was closed before everything could save we also check number of pokemons
            if(_allPokedexEntries!!.isNotEmpty() && _allPokedexEntries!!.size == NUMBER_OF_POKEMONS){
                showPokedexEntries()
                showLoadFromDbToast()
            }else{
                // TODO : if necessary get the list from the web (and cache it in the local database)
                // Get Pokedex entries
                lifecycleScope.launchWhenCreated {
                    val jobPokdexEntries = launch {
                        _allPokedexEntries  = getAllPokedexEntries()
                    }
                    jobPokdexEntries.join()
                }.invokeOnCompletion {
                    if (_allPokedexEntries == null){
                        showErrorLoadToast()
                        setLoadState(FAILED)
                    }else{
                        // Save TO DB
                        saveEntriesToDatabase()
                        // TODO : display the list in the adapter
                        showPokedexEntries()
                        showLoadFromAPIToast()
                    }
                }
            }
        }

    }

    /**
     * This method shows the pokedex entries with the help fo adapter.
     */
    private fun showPokedexEntries() {
        // TODO : pass the pokedex entries to the adapter
        adapter = PokedexRecyclerViewAdapter(this._allPokedexEntries!!)
        binding.pokedexRecyclerView.adapter = adapter
        binding.pokedexRecyclerView.layoutManager = LinearLayoutManager(this)
        setLoadState(COMPLETED)
    }

    /**
     * This method save pokedex entries to database
     */
    private fun saveEntriesToDatabase(){
        lifecycleScope.launchWhenCreated {
           launch(Dispatchers.IO) {
                database.pokedexEntryDao().insertEntry(_allPokedexEntries!!)
            }
        }
    }
}