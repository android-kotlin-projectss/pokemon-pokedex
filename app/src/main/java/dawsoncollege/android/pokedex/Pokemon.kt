package dawsoncollege.android.pokedex

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon_table")
data class Pokemon(
    @PrimaryKey val number: String,
    val name: String,
    val frontImgURL: String,
    val backImgURL: String,
    val types: String,
    val exp: String,
    val maxHp: String,
    val attStat: String,
    val defStat: String,
    val spAttStat: String,
    val spDefStat: String,
    val speedStat: String,
)

@Entity(tableName = "pokedex_entries")
data class PokedexEntry(
    @PrimaryKey val number: String,
    val name: String
)
