package dawsoncollege.android.pokedex.cache.interfaceDAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dawsoncollege.android.pokedex.Pokemon
import kotlinx.coroutines.flow.Flow

@Dao
interface IPokemonDAO {
    @Query("SELECT * FROM pokemon_table WHERE name = :pokeName")
    fun getPokemon(pokeName: String): Pokemon

    @Insert
    suspend fun insertPokemon(pokemon: Pokemon)

}