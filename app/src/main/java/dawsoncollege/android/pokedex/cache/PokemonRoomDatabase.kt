package dawsoncollege.android.pokedex.cache

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dawsoncollege.android.pokedex.PokedexEntry
import dawsoncollege.android.pokedex.Pokemon
import dawsoncollege.android.pokedex.cache.interfaceDAO.IPokedexEntryDAO
import dawsoncollege.android.pokedex.cache.interfaceDAO.IPokemonDAO

@Database(entities = [Pokemon::class, PokedexEntry::class], version = 1, exportSchema = false)
abstract class PokemonRoomDatabase: RoomDatabase() {
    abstract fun pokemonDao(): IPokemonDAO
    abstract fun pokedexEntryDao(): IPokedexEntryDAO

    companion object {
        @Volatile
        private var INSTANCE: PokemonRoomDatabase? = null

        fun getDatabase(context: Context): PokemonRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PokemonRoomDatabase::class.java,
                    "pokedex_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}