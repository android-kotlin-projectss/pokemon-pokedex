package dawsoncollege.android.pokedex.cache.interfaceDAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dawsoncollege.android.pokedex.PokedexEntry

@Dao
interface IPokedexEntryDAO {
    @Query("SELECT * FROM pokedex_entries")
    fun getAllEntries(): Array<PokedexEntry>


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntry(allEntries: Array<PokedexEntry>)


}